---
layout: handbook-page-toc
title: "Observability Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Who We Are

The following people are members of the Observability Team:

| Person | Role |
| ------ | ------ |
|[Kennedy Wanyangu](/company/team/#kwanyangu)|[Engineering Manager, Reliability](https://about.gitlab.com/job-families/engineering/infrastructure/engineering-management/#engineering-manager-reliability)|
|[Igor Wiedler](/company/team/#igorwwwwwwwwwwwwwwwwwwww)|[Staff Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Michal Wasilewski](/company/team/#mwasilewski-gitlab)|[Senior Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Natan Hoppe](/company/team/#nhoppe1)|[Senior Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|[Rehab Hassanein](/company/team/#rehab)|[Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|Open Position|[Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|Open Position|[Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|
|Open Position|[Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer/)|


## Mission

The __Observability team__ is responsible for building tooling that provides visibility via __metrics__, __logs__ and __tracing__ of services to enhance the availability, reliability, and performance of GitLab.com. The team appreciates that GitLab Team Members and GitLab.com's users both benefit from access to our metrics. We provide some metrics to GitLab users at https://dashboards.gitlab.com.


## Vision

The Observability team provides tooling that simplifies monitoring and debugging of services from an end-user perspective. The team supports engineering teams to identify __indicators__ of outages and service degradations, __debug__ the outages and degradations, __expose__ negative impacts of new functionality or changes and provide long-term trends for __planning__ purposes.

Short term goals:
- Build and manage monitoring tooling and storage of logs and metrics data
- Provide data that allows us to understand the past and current performance of our systems
- Provide monitoring data to engineering teams to improve debugging and developer velocity
- Ensure our monitoring data is consistently reliable, accurate, and available


## Indicators
The Observability team owns the following performance indicators:

1. [Mean Time Between Incident (MTBI)](/handbook/engineering/infrastructure/performance-indicators/#mean-time-between-incidents-mtbi)
2. [Mean Time To Resolution (MTTR)](/handbook/engineering/infrastructure/performance-indicators/#mean-time-to-resolution-mttr)
3. [Mean Time To Mitigate (MTTM)](/handbook/engineering/infrastructure/performance-indicators/#mean-time-to-mitigate-mttm)
4. [Mean Time Between Failures (MTBF)](/handbook/engineering/infrastructure/performance-indicators/#mean-time-between-failure-mtbf) Currently owned by __Scalability team__

## Responsibilities
The team focuses on the following tasks;
- Plan and scope on pre-planned units of work in providing more visibility into our SaaS offering of GitLab.com
- EOC Backlog - Quarterly milestones of corrective actions and EOC tooling maintenance.
- Customer Requests - Quarterly milestone of all small-scale tasks which originate from teams outside of Reliability and not tied to projects or corrective actions.


## Tenets

"If you can't measure it, you can't improve it." - __Peter Drucker__

- We provide data that allows us to understand the past and current performance of our systems.
- We make it easy to identify the current state of our systems.
- We provide data that helps us to predict the future state of our systems.
- Our data is consistently reliable, accurate, and available to all of GitLab.com's internal stakeholders.
