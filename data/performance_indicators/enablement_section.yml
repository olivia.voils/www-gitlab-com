- name: Enablement - Section PPI, Stage PPI - Page Load Time, p95 of requests from US
  base_path: "/handbook/product/performance-indicators/"
  definition: "95th percentile (p95) of requests from the United States. We have isolated by US to start, to avoid p95 being largely influenced by countries with slower internet connections back to the US where GitLab.com is hosted. This is due to the focus currently on addressing application performance issues rather than worldwide performance."
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  public: true
  pi_type: Section PPI, Stage PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
      - We have changed the metric, and are working to establish a target.
  implementation:
    status: Complete
    reasons:
      - We are working to [directly tie page load data to routes](https://gitlab.com/gitlab-org/gitlab/-/issues/331807). It looks like we can get a [viable first iteration](https://gitlab.com/gitlab-org/gitlab/-/issues/331807#note_684967888) via the [pseudonymization project](https://gitlab.com/groups/gitlab-org/-/epics/6551). Current ETA for this dependency is 14.4.
  lessons:
    learned:
    - The p95 is quite stable over the past few months. Without isolating for country, it is stable for longer. Believe the prior noise is due to the recent changes to better support region. Prior months may have very few requests properly annotated.
  urls:
      - https://app.periscopedata.com/app/gitlab/790506/gitlab.com-performance-per-snowplow-dashboard?widget=12056437&udv=0
      - https://app.periscopedata.com/app/gitlab/790506/gitlab.com-performance-per-snowplow-dashboard?widget=12057815&udv=0
  metric_name: performanceTiming
  sisense_data:
    chart: 12593953
    dashboard: 794513
    embed: v2
  sisense_data_secondary:
    chart: 10603331
    dashboard: 794513
    embed: v2

- name: Enablement:Distribution - Group PPI - Percentage of installations on the 3 most
    recent versions of GitLab
  base_path: "/handbook/product/performance-indicators/"
  definition: Of the total number of self-managed installs, what percentage are on
    one of the three most recent versions.
  target: 40%
  org: Enablement Section
  section: enablement
  stage: enablement
  group: distribution
  public: true
  pi_type: Group PPI 
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - A target of 40% has been set. 40% is the upgrade rate that was reached in early 2019.
    - Bottom line - % on latest 3 versions continues to be stable, eventhough the median age of version continues to generally increase. There was a noticable drop in median age of instance for CE users.
    - There is a need to continue to narrow down the factors that have the largest impact on a decision to upgrade by the instance owner. There are a few hypotheses for what affects the choice to upgrade, but it is difficult to tie those impacts directly to data points.
  implementation:
    status: Complete
    reasons:
    - Primary PPI is complete, and target is set.
  lessons:
    learned:
    - The [most recent update](https://app.periscopedata.com/app/gitlab/406972/?widget=8890933) to the percentage of users on the latest three versions shows a continued decrease in Helm installations and a decrease in source installations, after the large spike last month. The decrease in Helm is similar to the decrease that occurred around the last major release timeframe due to deprecations of older versions of items like Helm and Postgres. A potential reason for the [source installation activity](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/919) is the movement of Database Load Balancing to the free tier, which would be appealing to large open-source instances of GitLab.
    - .[Increasing the rate of upgrades for self-managed](https://gitlab.com/groups/gitlab-org/-/epics/5180) users after a major release is important as many new features and improvements are added. Therefore, we should [reach out to self-managed users](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/3989) especially after major upgrades through a blog or marketing campaign. Another avenue for outreach is to meet admins in their preferred lines of communication by [creating automated prompts for upgrades](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/843) so they can remain up to date on security and features.
    - Security was a central focus for admins to upgrade their version from the [ease of upgrading study](https://gitlab.com/gitlab-org/ux-research/-/issues/1114). The initial MVC has been merged, to [prompt users to sign up for the security newsletter](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/62502) in the next steps docs after installation. The next iteration, to [prompt current users to sign up for the security newsletter in the admin console](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/842), will be completed in the 14.4 milestone.
    - Work has begun on [better highlighting](https://gitlab.com/gitlab-org/gitlab/-/issues/295266) when instances are out of date to administrators. This will impact results once it has been completed. A dependent issue to [add JSON formatting](https://gitlab.com/gitlab-services/version-gitlab-com/-/issues/421) was found in 14.2 blocking this upgrade. T JSON formatting is scheduled to be completed in 14.4.
  metric_name: versions_behind_latest
  sisense_data:
    chart: 8658008
    dashboard: 406972
    embed: v2
  sisense_data_secondary:
    chart: 8890933
    dashboard: 406972
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=10278985&udv=1102166
  - https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=10279895&udv=1102166

- name: Enablement:Geo - Paid GMAU - Number of unique users utilizing a Geo secondary
  base_path: "/handbook/product/performance-indicators/"
  definition: Number of unique users utilizing a Geo secondary. This adoption metric helps us understand whether end users are actually seeing value in, and are using, geo secondaries.
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  group: geo
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    -  We've enabled [gathering usage data from Geo secondaries](https://gitlab.com/groups/gitlab-org/-/epics/4660) and have [added git fetch metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/298781) to the usage data. The data is [now available in Sisense](https://app.periscopedata.com/app/gitlab/885304/Geo-Node-Status-Usage-Data?widget=12066071&udv=0), and more work is needed to produce a usable chart. 
    - We also [added git push metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/320984) to the usage data. However we are [not seeing reported data](https://app.periscopedata.com/app/gitlab/885304/Geo-Node-Status-Usage-Data?widget=12066132&udv=0) and must investigate further.
    - GMAU is currently based on logging into the Secondary web interface. Git access is more common and we may now consider incorporating git fetch metrics now that they are instrumented. We also know that the UX of the secondary web interface is not good and we want to remove it, see [Opportunity Canvas(internal)](https://docs.google.com/document/d/1S27A6u134ASCZT_pcKHuxJrUA0aZybxfuHIci1FhYHg/edit#heading=h.4mt5fmtn0ax4). In order to assess the impact of our planned changes, having this (low) GMAU is really important. We've recently completed the feature work to make the secondary UI indistinguishable from the primary. Solution validation is in progress and can be tracked in the [secondary mimicry epic](https://gitlab.com/groups/gitlab-org/-/epics/1528).
  implementation:
    status: Instrumentation
    reasons:
    - Geo is not available on GitLab.com today, so cannot use Snowplow or the .com database.
  lessons:
    learned:
    - Geo GMAU has trended upwards since instrumentation. However, the overall number of users is still very low. This is likely because the WebUI is insufficient and we are not yet capturing users interacting with secondaries through git operations. We anticipate the upcoming release of an improved secondary UI experience and single URL support will have a positive impact on this metric.  
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/4660
  - https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics
  metric_name: geo_nodes
  sisense_data:
    chart: 10039214
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039565
    dashboard: 758607
    embed: v2

- name: Enablement:Geo - Paid GMAU - Number of potential Geo users
  base_path: "/handbook/product/performance-indicators/"
  definition: Sum of all users who are using instances with Geo enabled. All these users are positively impacted due to Geo in case of a failover or restore from backup. 
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  group: geo
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    -  For Disaster Recovery we are measuring *potential Geo users* - the number of active licenses. In an ideal world, no regular user would ever need to rely on Geo because there is no disaster. But if one occurs, everyone benefits. Based on [the node number distribution](https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics?widget=6471733&udv=0) ~60% of our customers use Geo mainly for DR. This is worth measuring because the customer must always make a conscious decision to enable Geo.
    - We are working on replicating and verifying all data types, so that Geo is on a solid foundation for both DR and Geo Replication.
    - With the release of replication for GitLab Pages in 14.3, Geo now replicates 100% of planned data types.
    - In 14.3, Geo verifies 50% (10 of 20) of data, not including unplanned data types. Verification for LFS Objects, External MR Diffs, Pages, and Uploads planned for 14.4 and 14.5.
  implementation:
    status: Instrumentation
    reasons:
    - Geo customers and user data available but we can't distinguish DR use case from replication yet.
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/4660
  - https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics
  metric_name: geo_nodes
  sisense_data:
    chart: 9939914
    dashboard: 500159
    embed: v2

- name: Enablement:Geo - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 2
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-geo/stage-groups-group-dashboard-enablement-geo)
    - Geo is not deployed on .com - This currently monitors one sidekiq worker that checks whether Geo is enabled.\
    - In August, the chart shows 100% error budget spent. This was due to a regression in the error calculation and [was resolved](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1276).
  sisense_data:
    chart: 12197844
    dashboard: 892802
    embed: v2
  sisense_data_secondary:
    chart: 12197848
    dashboard: 892802
    embed: v2

- name: Enablement:Memory - Group PPI - Memory Consumed
  base_path: "/handbook/product/performance-indicators/"
  definition: Average memory consumed by all invididual GitLab processes
  target: 1.5GB
  org: Enablement Section
  section: enablement
  stage: enablement
  group: memory
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  implementation:
    status: Complete
    reasons:
    - Plot displays a 13.13 release for unknown reasons. Investigating
    - Once the team switches back its focus from Redis to reducing memory consumption, we'll reevaluate the Group PPI, starting with updates to adjust the chart to include [how many of each process are started by default](https://gitlab.com/gitlab-com/Product/-/issues/1744).
  metric_name: topology.nodes[0].node_services
  sisense_data:
      chart: 10026240
      dashboard: 679200
      embed: v2
  health:
    level: 2
    reasons:
    - Memory consumption remains stable; 14.3 data too early to analyze.
    - We are pausing work on reducing memory consumption; the team will work on [Redis related issues](https://gitlab.com/gitlab-com/gl-infra/mstaff/-/issues/82) starting in 14.4 and for a few milestones.

- name: Enablement:Global Search - Paid GMAU - The number of unique paid users per month
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique active users and unique paid users interacting with either Basic Search or Advanced Search per month.
  target: 10% month over month (SaaS and self-managed combined)
  org: Enablement Section
  section: enablement
  stage: enablement
  group: global_search
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - August had double the growth in Paid SaaS unique users. This was likely caused by enabling of Advanced Search as a global default.  
      - It's likley that a large portion of this is traffic shifitng and not net new. 
      - This changed shifted 3% of the load from postgressQL to Elasticsearch. This Created benefit intotal it came with a trade off of Needing to [scale nodes in Elasticsearch](https://gitlab.com/gitlab-org/gitlab/-/issues/339489). 
  implementation:
    status: Complete
    reasons:
  lessons:
   learned:
    - We are focused on decreasing [page load times.](https://gitlab.com/gitlab-org/gitlab/-/issues/327106) This is a key deliverable to contiuned growth. 
  sisense_data:
    chart: 10039566
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039216
    dashboard: 758607
    embed: v2

- name: Enablement:Global Search - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 3
    reasons:
    - This is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-global_search/stage-groups-group-dashboard-enablement-global-search)
    - Error Budget Is underbudget for the first time since it was tracked.
  sisense_data:
    chart: 12197845
    dashboard: 892802
    embed: v2
  sisense_data_secondary:
    chart: 12197849
    dashboard: 892802
    embed: v2
    
- name: Enablement:Database - Group PPI - Database Query Apdex (100ms target, 250ms tolerable)
  base_path: "/handbook/product/performance-indicators/"
  definition: Database query Apdex, with 100ms the target and 250ms tolerable. This
    measures the ratio of queries which complete within the satisfactory time, informing
    how well the database is scaling and performing.
  target: 0.99
  org: Enablement Section
  section: enablement
  stage: enablement
  group: database
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - The query Apdex has decreased in all charts after the release of 13.10, including [the weekly one](https://app.periscopedata.com/app/gitlab/754160/Enablement::Database---Performance-Indicators?widget=9885649&udv=0), which has a pretty constant number of instances reporting stats. No similar trend on the GitLab.com charts, nor any other clear signal indicating what may be affecting the Apdex.
    - This is still present in 14.2. We plan to [investigate](https://gitlab.com/gitlab-org/database-team/team-tasks/-/issues/199) this, but the team has not focused on an investigation due to capacity constraints; current focus is still on mitigating the Primary Key Overflow risk and addressing stability concerns on GitLab.com.
    - Our plan to address query performance growing pains is to start working towards reducing the [size of individual tables](https://gitlab.com/groups/gitlab-org/-/epics/6751). We plan to also work on making the new [batched background migrations framework](https://gitlab.com/groups/gitlab-org/-/epics/6751) the standard for all database migrations, which should help reduce database related incidents steming from background processes asynchrously performing updates in bulk.
    - We expect our work on [automated database migration testing](https://gitlab.com/groups/gitlab-org/database-team/-/epics/6) to indirectly improve the database performance by minimising the number of database related incidents during deployments.
  implementation:
    status: Complete
    reasons:
    - The [PPI has been instrumented](https://gitlab.com/gitlab-org/gitlab/-/issues/227305) in 13.4.
  lessons:
    learned:
    - Apdex on GitLab.com exceeds our group PPI (see [100ms - Tolerable 250ms](https://tinyurl.com/64e6acku) and [50ms - Tolerable 100ms](https://tinyurl.com/4mjw5azv) for master) but reflects with sharp drops the production incidents that related to the database.
  metric_name: query_apdex_weekly_average
  sisense_data:
    chart: 9885641
    dashboard: 754160
    embed: v2
  sisense_data_secondary:
    chart: 10091150
    dashboard: 754160
    embed: v2
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/227305
